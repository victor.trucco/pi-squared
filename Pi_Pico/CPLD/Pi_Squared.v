//
// Pi Pico adapter
// Copyright (c) 2022 - Victor Trucco
//
// All rights reserved
//
// Redistribution and use in source and synthezised forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// Redistributions in synthesized form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
//
// Neither the name of the author nor the names of other contributors may
// be used to endorse or promote products derived from this software without
// specific prior written permission.
//
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// You are responsible for any legal issues arising from your use of this code.
//


`timescale 1ns / 100ps
`default_nettype none

module Pi_Squared 
(
		// Clocks
		input  wire clock_i,

		output wire led_red,
		output wire led_yellow,
		output wire led_green,
		
		// MSX BUS
		inout  reg  [7:0] bus_data_io,
		input  wire [15:0] bus_addr_n_i,
		input  wire bus_mreq_n_i,		
		input  wire bus_ioreq_n_i,
		input  wire bus_wr_n_i,
		input  wire bus_rd_n_i,
		input  wire bus_rst_n_i,
		input  wire bus_dir_i,
		input  wire bus_m1_n_i,
		output wire bus_wait_n_o,		
		output wire bus_int_n_o,	
		input  wire bus_rfsh_n_i,	
		input  wire bus_cs1_n_i,	
		input  wire bus_cs2_n_i,	
		input  wire bus_sltsl_n_i,
		input  wire bus_clock_i,
		output reg  bus_snd_o,
		
		// Pico GPIO BUS
		inout  wire [25:0] pico_gpio, // 0-22 are 0-22 in pico, 23/24/25 are 26/27/28 in pico
		
		//external GPIO pins
  	   inout  wire [4:1] ext_io 
		
);


	reg [14:0] latch_a_s;
	reg [ 7:0] latch_r_s;
	reg [ 7:0] latch_w_s;
	reg [ 7:0] bus_d_s;
	
	wire trigger_s;
	wire io_wr, io_rd;
	wire mem_wr, mem_rd;
	
	assign io_wr = (bus_ioreq_n_i == 1'b0 && bus_wr_n_i == 1'b0);
	assign io_rd = (bus_ioreq_n_i == 1'b0 && bus_rd_n_i == 1'b0);
	
	assign mem_wr = (bus_mreq_n_i == 1'b0 && bus_wr_n_i == 1'b0);
	assign mem_rd = (bus_mreq_n_i == 1'b0 && bus_rd_n_i == 1'b0);
	
	assign pico_gpio[7:0] = ((pico_gpio[24] == 1'b0)) ? 8'hzz : latch_w_s;
	assign pico_gpio[23:8] = bus_addr_n_i;
	
	//assign pico_gpio[24] = bus_wr_n_i;
	
	
	assign pico_gpio[25] = (
							( io_wr && bus_addr_n_i[7:0] == 8'h70)    //write on 0x70   
							) ? 1'b0 : 1'b1;
	
	assign ext_io[1] = bus_wr_n_i;
	assign ext_io[2] = bus_rd_n_i;
	assign ext_io[3] = bus_mreq_n_i;
	assign ext_io[4] = bus_ioreq_n_i;
	
	assign trigger_s =(
							((io_rd || io_wr) && bus_addr_n_i[7:0] == 8'h70)    //write on 0x70   
							) ? 1'b0 : 1'b1;

	
	always @ (*)
	begin
			//if (bus_wr_n_i == 1'b0 && trigger_s == 1'b0) latch_d_s = pico_gpio[7:0];	
			if (bus_rd_n_i == 1'b0 && trigger_s == 1'b0) bus_data_io = latch_r_s; else bus_data_io = 8'bZZZZZZZZ;
			
			if (bus_wr_n_i == 1'b0 && trigger_s == 1'b0) latch_w_s = bus_data_io;
			
			if (pico_gpio[24] == 1'b0)	latch_r_s = pico_gpio[7:0];
	end


	
	//debug LEDs
	assign led_red    = latch_w_s[0];
	assign led_yellow = latch_w_s[1];
	assign led_green  = latch_w_s[2];
	

	// release the MSX BUS
	assign bus_wait_n_o = 1'bZ;
	assign bus_int_n_o = 1'bZ; 
	assign pico_gpio[24] = 1'bz; // to read the gpio[24]

endmodule