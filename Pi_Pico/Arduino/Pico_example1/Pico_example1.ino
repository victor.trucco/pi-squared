//
// Pi Pico example using the Arduino IDE
// Copyright (c) 2022 - Victor Trucco
//
// All rights reserved
//
// Redistribution and use in source and synthezised forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// Redistributions in synthesized form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
//
// Neither the name of the author nor the names of other contributors may
// be used to endorse or promote products derived from this software without
// specific prior written permission.
//
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// You are responsible for any legal issues arising from your use of this code.
//


void isr1()
{
   unsigned char val;
   
   digitalWrite(27, HIGH);
   digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on 
   gpio_set_dir_in_masked(0xff);   // GPIO0-7 as inputs 
   delay(1);

   //get the BUS value
   val = (digitalRead(7)<<7) | (digitalRead(6)<<6) | (digitalRead(5)<<5) | (digitalRead(4)<<4) | (digitalRead(3)<<3) | (digitalRead(2)<<2) | (digitalRead(1)<<1) | digitalRead(0);       // get GPIO0-7

   gpio_set_dir_out_masked(0xff);   // GPIO0-7 as outputs 

  // divide the initial value by 2, shifting the bits
  digitalWrite(7, LOW);
  digitalWrite(6, (val>>7)&1);
  digitalWrite(5, (val>>6)&1);
  digitalWrite(4, (val>>5)&1);
  digitalWrite(3, (val>>4)&1);
  digitalWrite(2, (val>>3)&1);
  digitalWrite(1, (val>>2)&1);
  digitalWrite(0, (val>>1)&1);

   //signalize to the CPLD, with a LOW pulse
   digitalWrite(27, LOW);
   delay(1);
   digitalWrite(27, HIGH);
   
   gpio_set_dir_in_masked(0xff);   // GPIO0-7 as inputs 
      
   digitalWrite(LED_BUILTIN, LOW);    // turn the LED off 
   
}

void setup() {

  pinMode (27, OUTPUT);
  digitalWrite(27, HIGH);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off

  //Set the Interrupt service
  pinMode(28, INPUT_PULLUP);
  attachInterrupt(28 , isr1 , FALLING);
}

void loop() {
  // Nothing to see here
}
